// State machine for the IMM communication interface
package main

import (
	"bitbucket.org/mfkenney/go-stats"
	"bitbucket.org/uwaploe/go-dputil"
	"container/list"
	"encoding/json"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"log"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// Error triggered by an invalid state change
type StateError struct {
	msg string
}

func (e *StateError) Error() string {
	return e.msg
}

type QueueEntry struct {
	Id   string `json:"id"`
	Body string `json:"body"`
}

type Handler func(*ImmState) string

type Fsm struct {
	Handlers   map[string]Handler
	StartState string
	EndStates  map[string]bool
}

// Dump command response time metrics to a Redis hash table.
func dump_metrics(conn redis.Conn, data map[string]*stats.Stats) {
	key := "command_stats"
	conn.Send("MULTI")
	for k, v := range data {
		conn.Send("HSET", key, k, v.String())
	}
	_, err := conn.Do("EXEC")
	if err != nil {
		log.Println(err)
	}
}

var imm_setup Handler = func(ms *ImmState) string {
	ms.sw.On()
	ms.imm.SetTimeout(time.Second * 5)

	log.Println("Power on")
	time.Sleep(time.Duration(ms.cfg.Warmup) * time.Millisecond)
	ms.err = ms.imm.Wakeup()

	if ms.err != nil {
		return "error"
	}

	for k, v := range ms.cfg.Settings {
		ms.err = ms.imm.Set(k, v)
		if ms.err != nil {
			return "error"
		}
	}

	if ms.peer_sn == "" {
		return "discover"
	} else {
		log.Println("Skipping peer discovery phase")
		return "mainloop"
	}
}

var imm_discover Handler = func(ms *ImmState) string {
	var peers []string
	var next string

	next = "mainloop"
	peers, ms.err = ms.imm.FindPeers()
	if peers != nil {
		log.Printf("Found peer: %s", peers[0])
		ms.peer_sn = peers[0]
	} else {
		// Run the peer search in a goroutine so it can be
		// interrupted.
		cs := make(chan string)
		ticker := time.NewTicker(
			time.Duration(ms.cfg.Discover.Interval) * time.Millisecond)
		defer ticker.Stop()

		go func() {
			tries := ms.cfg.Discover.Tries
			for _ = range ticker.C {
				peers, ms.err = ms.imm.FindPeers()
				if peers != nil {
					log.Printf("Found peer: %s", peers[0])
					ms.peer_sn = peers[0]
					cs <- "mainloop"
				}
				tries--
				if tries <= 0 {
					ms.err = fmt.Errorf("No peers found")
					cs <- "error"
				}
			}
		}()

		select {
		case next = <-cs:
		case <-ms.cancel:
			ticker.Stop()
			next = "shutdown"
		}
	}

	return next
}

var imm_mainloop Handler = func(ms *ImmState) string {
	var hostid int
	var next string
	var interval time.Duration

	ms.err = ms.imm.CaptureLine(true)
	if ms.err != nil {
		return "error"
	}

	hostid, ms.err = strconv.Atoi(ms.peer_sn)
	if ms.err != nil {
		return "error"
	}

	cs := make(chan string)
	interval = time.Duration(10) * time.Second
	ticker := time.NewTicker(interval)
	defer ticker.Stop()

	cmds := list.New()
	go func() {
		var (
			slot  int
			cmd   string
			err   error
			resp  string
			parts []string
		)
		defer close(cs)
		conn := ms.pool.Get()
		defer conn.Close()

		entry := QueueEntry{}
		ev := dputil.Event{Name: "reply"}

		for t := range ticker.C {
			slot = int((t.Unix() / 10) % 10)

			// Collect scheduled commands
			for _, c := range ms.cmds[slot] {
				cmds.PushBack(c)
			}

			// Get the next command from the Redis queue
			cmd, err = redis.String(conn.Do("LPOP", ms.cfg.Queues["command"]))
			if err == nil {
				if cmd == "halt" {
					cs <- "shutdown"
					break
				} else {
					log.Printf("New command from queue: %q", cmd)
					cmds.PushBack(cmd)
				}
			}

			// Send each command and process the response
			for cmds.Len() > 0 {
				cmd = cmds.Remove(cmds.Front()).(string)
				// The commands from the Redis queue can be either
				// a JSON encoded QueueEntry or simply a string.
				err = json.Unmarshal([]byte(cmd), &entry)
				if err != nil {
					entry.Id = ""
					entry.Body = cmd
				}
				tsend := time.Now()
				resp, err = ms.imm.HostMessage(hostid, entry.Body)
				elapsed := time.Since(tsend)

				if err != nil {
					if ms.strict {
						// When strict error checking is enabled, we transition to
						// the error state, otherwise move on to the next command.
						ms.err = err
						cs <- "error"
					} else {
						log.Printf("HostMessage error: %v", err)
						continue
					}
				}

				// Update the response-time statistics
				if ms.tresponse[entry.Body] == nil {
					ms.tresponse[entry.Body] = stats.NewStats()
				}
				ms.tresponse[entry.Body].Update(elapsed.Seconds())

				// If the entry had an "id", publish the reply on
				// the events.mmp channel.
				if entry.Id != "" {
					ev.Attrs["id"] = entry.Id
					// An Event attribute must not contain any spaces
					// so we URL-encode the "resp" attribute just
					// in case ...
					ev.Attrs["resp"] = url.QueryEscape(resp)
					ev.Attrs["t"] = time.Now().Unix()
					ms.conn.Do("PUBLISH", "events.mmp", ev.String())
				}

				// All responses from the DPC have the form TAG:CONTENTS<CR>
				parts = strings.SplitN(resp, ":", 2)
				if len(parts) == 2 {
					switch parts[0] {
					case "I":
						log.Println(strings.TrimRight(parts[1], " \r\n"))
					case "S":
						process_status(strings.TrimRight(parts[1], " \r\n"), ms)
					case "D":
						process_sensor(strings.TrimRight(parts[1], " \r\n"), ms)
					case "V":
						process_event(strings.TrimRight(parts[1], " \r\n"), ms)
					case "B":
						process_battery(strings.TrimRight(parts[1], " \r\n"), ms)
					case "E":
						log.Println("Database entry")
					}
				} else {
					log.Printf("ERROR:Invalid response: %q", resp)
				}

				dump_metrics(ms.conn, ms.tresponse)
			}
		}
	}()

	select {
	case next = <-cs:
	case <-ms.cancel:
		ticker.Stop()
		next = "shutdown"
	}

	return next
}

var imm_shutdown Handler = func(ms *ImmState) string {
	err := ms.imm.Sleep()
	if err != nil {
		log.Printf("Cannot put IMM to sleep: %v\n", err)
	}
	time.Sleep(time.Millisecond * 250)
	ms.sw.Off()
	log.Printf("Power off\n")
	return "done"
}

var imm_error Handler = func(ms *ImmState) string {
	log.Printf("Error: %v\n", ms.err)
	return "shutdown"
}

func (fsm *Fsm) AddState(name string, fn Handler) {
	fsm.Handlers[name] = fn
}

func (fsm *Fsm) AddEndState(name string) {
	fsm.EndStates[name] = true
}

func (fsm *Fsm) Run(ms *ImmState) {
	handler := fsm.Handlers[fsm.StartState]
	for {
		next := handler(ms)
		log.Printf("Next state = %q\n", next)
		ms.conn.Do("HSET", "imm", "state", next)
		_, done := fsm.EndStates[next]
		if done {
			break
		} else {
			handler = fsm.Handlers[next]
		}
	}
}

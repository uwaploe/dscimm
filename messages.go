// Process messages from the IMM communication interface.
package main

import (
	"bitbucket.org/uwaploe/go-dpcmsg"
	"bitbucket.org/uwaploe/go-dputil"
	"encoding/base64"
	"encoding/json"
	"github.com/gogo/protobuf/proto"
	"log"
	"time"
)

// Process the reply to a sensor data request
func process_sensor(rawdata string, ms *ImmState) {
	cont := dpcmsg.SensorData{}
	enc := base64.StdEncoding

	data, err := enc.DecodeString(rawdata)
	if err != nil {
		log.Println(err)
		return
	}

	err = proto.Unmarshal(data, &cont)
	if err != nil {
		log.Println(err)
		return
	}

	// Do not process data records older than the last sample
	recs := make([]*dputil.DataRecord, 0)
	for _, rec := range cont.ToDataRecords() {
		if rec.T.After(ms.tscache[rec.Source]) {
			recs = append(recs, rec)
			ms.tscache[rec.Source] = rec.T
		}
	}

	// Add each data record to the Archiver queue for storage
	// then publish a JSON version on the data.sci channel.
	for _, dr := range recs {
		v := dr.Serialize()
		err = ms.dataq.Put(v...)
		if err != nil {
			log.Printf("Cannot queue data record: %v\n", err)
		}
		s, err := json.Marshal(*dr)
		if err != nil {
			log.Printf("JSON conversion failed: %v\n", dr)
		} else {
			ms.conn.Do("PUBLISH", "data.sci", s)
		}
	}
}

// Process the reply to a status request
func process_status(rawdata string, ms *ImmState) {
	rec := &dpcmsg.Status{}
	enc := base64.StdEncoding

	data, err := enc.DecodeString(rawdata)
	if err != nil {
		log.Println(err)
		return
	}

	err = proto.Unmarshal(data, rec)
	if err != nil {
		log.Println(err)
		return
	}

	pr := rec.ToDataRecord()
	d := pr.Data.(map[string]interface{})
	pnum := d["profile"].(uint32)

	// Is this the start of a new profile?
	if pnum > ms.pnum {
		// Signal the end of the current profile ...
		ev := &dputil.Event{
			Name:  "profile:end",
			Attrs: make(map[string]interface{})}
		ev.Attrs["t"] = int64(time.Now().Unix())
		ev.Attrs["pnum"] = int64(ms.pnum)
		log.Println(ev)
		ms.conn.Do("PUBLISH", "events.mmp", ev.String())

		// ... and the start of a new one.
		ev.Name = "profile:start"
		ev.Attrs["pnum"] = int64(pnum)
		log.Println(ev)
		ms.conn.Do("PUBLISH", "events.mmp", ev.String())

		ms.pnum = pnum
	}

	// Add the Profiler data record to the Archiver queue for
	// storage then publish a JSON version on the data.eng
	// channel.
	v := pr.Serialize()
	err = ms.dataq.Put(v...)
	if err != nil {
		log.Printf("Cannot queue data record: %v\n", err)
	}

	s, err := json.Marshal(*pr)
	if err != nil {
		log.Printf("JSON conversion failed: %v\n", pr)
	} else {
		ms.conn.Do("PUBLISH", "data.eng", s)
	}
}

// Process the reply to an event request
func process_event(rawdata string, ms *ImmState) {
	ev := dputil.ParseEvent(rawdata)
	tstamp := time.Unix(ev.Attrs["t"].(int64), int64(0))

	// Is this event more recent than the last?
	if tstamp.After(ms.tscache["evlogger"]) {
		ms.tscache["evlogger"] = tstamp

		if ev.Name == "profile:start" {
			// Only publish a profile:start event message if this
			// is a new profile.
			pnum := uint32(ev.Attrs["pnum"].(int64))
			if pnum > ms.pnum {
				ms.conn.Do("PUBLISH", "events.mmp", rawdata)
				ms.pnum = pnum
			}
		} else {
			// Publish all other events unconditionally.
			ms.conn.Do("PUBLISH", "events.mmp", rawdata)
		}
	}
}

// Process the reply to a battery request
func process_battery(rawdata string, ms *ImmState) {
	rec := &dpcmsg.Battery{}
	enc := base64.StdEncoding

	data, err := enc.DecodeString(rawdata)
	if err != nil {
		log.Println(err)
		return
	}

	err = proto.Unmarshal(data, rec)
	if err != nil {
		log.Println(err)
		return
	}

	dr := rec.ToDataRecord()

	if dr.T.After(ms.tscache[dr.Source]) {
		// Add the Battery data record to the Archiver queue for
		// storage then publish a JSON version on the data.eng
		// channel.
		v := dr.Serialize()
		err = ms.dataq.Put(v...)
		if err != nil {
			log.Printf("Cannot queue data record: %v\n", err)
		}

		s, err := json.Marshal(*dr)
		if err != nil {
			log.Printf("JSON conversion failed: %v\n", dr)
		} else {
			ms.conn.Do("PUBLISH", "data.eng", s)
		}
		ms.tscache[dr.Source] = dr.T
	}
}

// This program is part of the Deep Profiler project and manages the
// IMM interface on the DSC
package main

import (
	"bitbucket.org/mfkenney/go-power"
	"bitbucket.org/mfkenney/go-redisq"
	"bitbucket.org/mfkenney/go-stats"
	"bitbucket.org/uwaploe/go-imm"
	"bufio"
	"flag"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/garyburd/redigo/redis"
	"github.com/tarm/goserial"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"
)

// Default configuration
var defcfg = `
port:
  device: /dev/ttymxc2
  baud: 9600
  switch: "131"
warmup: 1000
discover:
  interval: 30000
  tries: 60
settings:
  thost0: 0
  thost1: 0
  thost2: 500
  thost3: 1000
  tmodem2: 600
  tmodem3: 1100
  enableecho: 0
  enablehostwakeupcr: 0
  enablehostpromptconfirm: 0
  enablefullpwrtx: 1
  enablebackspace: 0
  termtohost: 254
  termfromhost: 254
queues:
  command: "dpc:commands"
  data: "archive:queue"
commands:
  - text: status
    slots: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
  - text: ev
    slots: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
  - text: sens ctd_1 optode_1
    slots: [0, 2, 4, 6, 8]
  - text: sens flntu_1 flcd_1
    slots: [1, 3, 7, 9]
  - text: sens acm_1
    slots: [5]
`

var Version = "dev"
var BuildDate = "unknown"

// Peer discovery settings
type DiscoverCfg struct {
	// Check interval in milliseconds
	Interval int
	// Number of attempts before giving up
	Tries int
}

// Commands for the DPC
type CommandsCfg struct {
	Text  string
	Slots []uint
}

type ProgCfg struct {
	// Serial and power interface description
	Port map[string]interface{}
	// Warm-up time in milliseconds
	Warmup int
	// Peer discovery settings
	Discover DiscoverCfg
	// IMM configuration settings
	Settings map[string]interface{}
	// Redis queues (lists)
	Queues map[string]string
	// Scheduled commands
	Commands []CommandsCfg
}

type ImmState struct {
	imm       *imm.IMM
	pool      *redis.Pool
	conn      redis.Conn
	sw        power.PowerSwitch
	err       error
	cfg       ProgCfg
	peer_sn   string
	pnum      uint32
	strict    bool
	dataq     *redisq.Queue
	cmds      [10][]string
	statefile string
	cancel    chan struct{}
	tscache   map[string]time.Time
	tresponse map[string]*stats.Stats
}

func new_pool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		MaxActive:   16,
		IdleTimeout: 120 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func save_state(ms *ImmState) {
	if ms.statefile != "" {
		savecfg := map[string]interface{}{
			"pnum":    ms.pnum,
			"tscache": ms.tscache,
		}
		f, err := os.Create(ms.statefile)
		defer f.Close()
		if err != nil {
			log.Printf("Cannot save state: %v", err)
		} else {
			err = toml.NewEncoder(bufio.NewWriter(f)).Encode(savecfg)
			if err != nil {
				log.Printf("Encoding error: %v", err)
			} else {
				log.Printf("State saved to %s", ms.statefile)
			}
		}
	}
}

func load_state(ms *ImmState, filename string) {
	savecfg := make(map[string]interface{})
	ms.statefile = filename
	_, err := toml.DecodeFile(filename, &savecfg)
	if err != nil {
		log.Printf("Cannot decode state file: %v", err)
	} else {
		var ok bool
		x, ok := savecfg["pnum"].(int64)
		if !ok {
			log.Printf("Type mismatch: %T", savecfg["pnum"])
		} else {
			ms.pnum = uint32(x)
		}
		tscache, ok := savecfg["tscache"].(map[string]interface{})
		if !ok {
			log.Printf("Type mismatch: %T", savecfg["tscache"])
		} else {
			for k, v := range tscache {
				ms.tscache[k], ok = v.(time.Time)
			}
		}
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options]\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "Manage the IMM interface on the DSC\n\n")
		flag.PrintDefaults()
	}

	showvers := flag.Bool("version", false,
		"Show program version information and exit")
	cfgfile := flag.String("cfgfile", "",
		"Path to the configuration file")
	sched := flag.Bool("schedule", true,
		"Set to 'false' to disable the scheduled commands")
	strict := flag.Bool("strict", true,
		"Set to 'false' to disable strict error checking")
	peer := flag.String("peer", "",
		"Serial number of peer (DPC) IMM")
	state := flag.String("state", "",
		"File to load/store state information")

	flag.Parse()
	if *showvers {
		fmt.Printf("dscimm %s\n", Version)
		fmt.Printf("  Build date: %s\n", BuildDate)
		fmt.Printf("  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	var contents []byte
	var err error

	if *cfgfile != "" {
		contents, err = ioutil.ReadFile(*cfgfile)
		if err != nil {
			panic(err)
		}
	} else {
		contents = []byte(defcfg)
	}

	cfg := ProgCfg{}
	err = yaml.Unmarshal(contents, &cfg)
	if err != nil {
		panic(err)
	}

	// Open serial port
	c := &serial.Config{Name: cfg.Port["device"].(string),
		Baud: cfg.Port["baud"].(int)}
	s, err := serial.OpenPort(c)
	if err != nil {
		panic(err)
	}

	// Power switch
	sw := power.NewTsPowerSwitch(cfg.Port["switch"].(string))
	if sw == nil {
		log.Fatalf("Cannot access power switch!")
	}
	// Make sure the power is off
	sw.Off()

	ms := ImmState{
		imm:       imm.NewIMM(s),
		sw:        sw,
		cfg:       cfg,
		strict:    *strict,
		pool:      new_pool("localhost:6379"),
		cancel:    make(chan struct{}, 1),
		tscache:   make(map[string]time.Time),
		tresponse: make(map[string]*stats.Stats),
	}

	// Add scheduled commands
	if *sched {
		for _, c := range cfg.Commands {
			for _, t := range c.Slots {
				if ms.cmds[t] == nil {
					ms.cmds[t] = make([]string, 1)
					ms.cmds[t][0] = c.Text
				} else {
					ms.cmds[t] = append(ms.cmds[t], c.Text)
				}
			}
		}
	}

	// Load saved state
	if *state != "" {
		load_state(&ms, *state)
		defer save_state(&ms)
	}

	log.Println("Scheduled commands")
	for i, c := range ms.cmds {
		log.Printf("[%d]=%q\n", i, c)
	}

	// Set serial number of the peer IMM. Doing this will cause the
	// program to skip the peer discovery phase.
	if *peer != "" {
		ms.peer_sn = *peer
	}

	// Create a non-pool Redis connection to log process status.
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		panic(err)
	}
	ms.conn = conn

	// Initialize the data queue which will be read by the archiver.
	ms.dataq, err = redisq.NewQueue(ms.conn, cfg.Queues["data"], 200)
	if err != nil {
		panic(err)
	}

	// Log our process ID
	conn.Do("HSET", "imm", "pid", os.Getpid())

	cancel := make(chan struct{})
	ms.cancel = cancel

	// Initialize the FSM
	machine := Fsm{StartState: "setup"}
	machine.Handlers = make(map[string]Handler)
	machine.EndStates = make(map[string]bool)
	machine.AddState("setup", imm_setup)
	machine.AddState("discover", imm_discover)
	machine.AddState("mainloop", imm_mainloop)
	machine.AddState("error", imm_error)
	machine.AddState("shutdown", imm_shutdown)
	machine.AddEndState("done")

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close. If a signal arrives, close the "cancel" channel
	// to cleanly shutdown the FSM.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			close(ms.cancel)
		}
	}()

	machine.Run(&ms)
}
